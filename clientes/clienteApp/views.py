from django.shortcuts import render, redirect, get_object_or_404
from .models import *
from .forms import *

# CRUD

#Create
def client_new(request, template_name='client_form.html'):
    form = ClientForm(request.POST or None, request.Files or None)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('client_list')
    return render(request, template_name, dados)

#Read
def client_list(request, template_name='client.html'):
    clients = Client.objects.all()
    dados = {'clients': clients}
    return render(request, template_name, dados)

#Update
def client_update(request, id,template_name='client_form.html'):
    client = get_object_or_404(Client, pk=id)
    form = ClientForm(request.POST or None, request.Files or None, instance=client)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('client_list')

    return render(request, template_name, dados)

#Delete
def client_delete(request, id, template_name='client_delete.html'):
    client = get_object_or_404(Client, pk=id)

    if request.method == 'POST':
        client.delete()
        return redirect('client_list')